# Forest Fire Prediction Model
### By- Aarush Kumar
Forest Fire Prediction Model using RandomForestClassifier Model.
A wildfire, wildland fire or rural fire is an uncontrolled fire in an area of combustible vegetation occurring in rural areas. Depending on the type of vegetation present, a wildfire can also be classified more specifically as a brush fire, bushfire (in Australia), desert fire, forest fire, grass fire, hill fire, peat fire, vegetation fire, or veld fire.
As forestfire causes devastating affects so in this model I implemented RandomForestClassifier Model in order to make predictions for forest fire,and my model gained an accuracy of around 96%.
## Steps followed:
### Loading the data-
![Screenshot_from_2021-05-28_06-15-02](/uploads/2345fb4451298e51b3ed437ff5dfffb2/Screenshot_from_2021-05-28_06-15-02.png)
### Visualization-
![Screenshot_from_2021-05-28_06-15-30](/uploads/19e1c9ea93c7fd4e8693d61910e32d64/Screenshot_from_2021-05-28_06-15-30.png)
### Corralation-
![Screenshot_from_2021-05-28_06-15-48](/uploads/8f0c7bc60679c5f925829e698266ca89/Screenshot_from_2021-05-28_06-15-48.png)
### Testing model accuracy and finding Confusion Matrix-
![Screenshot_from_2021-05-28_06-16-05](/uploads/7f0395c45b44fc152c48209ef8c18026/Screenshot_from_2021-05-28_06-16-05.png)
## Thankyou ...
