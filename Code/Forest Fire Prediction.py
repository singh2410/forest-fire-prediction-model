#!/usr/bin/env python
# coding: utf-8

# # Forest Fire Prediction Model using RandomForest Model.
# #By- Aarush Kumar
# #Dated: May 28,2021

# In[23]:


import numpy as np
import pandas as pd
import seaborn as sb
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[24]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Forest Fire Prediction/data.csv')


# In[25]:


df


# In[26]:


df.head()


# In[27]:


print(df.columns)


# In[28]:


df.shape


# In[29]:


df.isnull().sum()


# In[30]:


df.describe()


# In[34]:


#About Target/Cover_Type variable 
df.Cover_Type.value_counts()


# In[35]:


#Take some column
col = ['Elevation', 'Aspect', 'Slope', 'Horizontal_Distance_To_Hydrology',
       'Vertical_Distance_To_Hydrology', 'Horizontal_Distance_To_Roadways',
       'Hillshade_9am', 'Hillshade_Noon', 'Hillshade_3pm',
       'Horizontal_Distance_To_Fire_Points']


# In[36]:


train = df[col]


# In[37]:


#histogram
train.hist(figsize=(13, 11))
plt.show()


# In[38]:


#Boxplot
plt.style.use('ggplot')
for i in col:
    plt.figure(figsize=(13, 7))
    plt.title(str(i) + " with " + str('Cover_Type'))
    sb.boxplot(x=df.Cover_Type, y=train[i])
    plt.show()


# In[40]:


#Corralation
plt.figure(figsize=(13, 9))
corr = train.corr()
sb.heatmap(corr, annot=True)
plt.show()


# In[41]:


from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel


# In[42]:


#separate features and target
feature = df.iloc[:, :54]
y = df.iloc[:, 54] 


# In[43]:


# Features Reduction
ETC = ExtraTreesClassifier()
ETC = ETC.fit(feature, y)


# In[44]:


model = SelectFromModel(ETC, prefit=True)
X = model.transform(feature)


# In[45]:


#shape of new feature
X.shape


# In[46]:


#Split the data into test and train formate
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=10)


# In[47]:


X_train


# In[49]:


y_train


# In[50]:


#Random Forest
from sklearn.ensemble import RandomForestClassifier
RFC = RandomForestClassifier(n_estimators=100)


# In[51]:


#fit
RFC.fit(X_train, y_train)


# In[52]:


#prediction
y_pred = RFC.predict(X_test)


# In[53]:


#score
print("Accuracy of model is ", RFC.score(X_test, y_test)*100)


# In[54]:


#confusion matrix
cm = confusion_matrix(y_pred, y_test)
plt.figure(figsize=(13, 9))
sb.set(font_scale=1.2)
sb.heatmap(cm, annot=True, fmt='g')
plt.show()

